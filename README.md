# cupla-docker

This repository contains docker images for GitLab CI tests for [cupla](https://gitlab.com/hzdr/cupla).

All Dockerfiles are based on ubuntu Dockerfiles.

# Add a new container                                                                                                  
                                                                                                                       
1. add a folder to the root directory that has the name of the image
2. create `Dockerfile` in new folder
3. add a new entry to the `.gitlab-ci.yml` with the following content:
                                                                                                                       
```yaml
<name of folder>:
  variables:
    IMAGE: <name of folder>
  extends: .base_job
```